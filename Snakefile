import os
import glob
import platform

from snakemake.utils import validate


ENUMS = ['01', '02', '03']

rule all:
    input:
        expand("data/compiled-data-{enum}.csv", enum=ENUMS),
        expand("docs/artifacts/workflow-artifact-{enum}.md", enum=ENUMS)

rule data_preprocessing:
    input:
        lambda wildcard: glob.glob(f"data/{wildcard.enum}/*.csv")
    output:
        "data/compiled-data-{enum}.csv"
    threads: 4
    script:
        "src/snakemake/data-preprocessor.py"

rule train_models:
    input:
        "data/compiled-data-{enum}.csv"
    output:
        "docs/artifacts/workflow-artifact-{enum}.md"
    threads: 4
    conda:
        ".envs/ml.yml"
    shell:
        "python src/trees-training-workflow.py --file {input} --artifact {output}"
