import os
import glob
import platform

from snakemake.utils import validate


# Скачивание датасета с Kaggle и загрузка в s3 для последующего импорта
rule downloading:
    run:
        commands = [
            'kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data -p data/.tmp --unzip',
            'aws s3 cp data/.tmp/2015-street-tree-census-tree-data.csv s3://data',
        ]
        for c in commands:
            shell(c)

# Импорт файла данных в LakeFS и коммит
rule init_raw_data:
    run:
        commands = [
            'lakectl import --from s3://data/2015-street-tree-census-tree-data.csv --to lakefs://data/main/ -m "NY tree dataset init"',
            # Удаляем исходный файл в S3, который был нужен только для импорта в LakeFS
            'aws s3 rm s3://data/2015-street-tree-census-tree-data.csv'
       ]
        for c in commands:
            shell(c)
