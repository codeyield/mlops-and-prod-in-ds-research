from fastapi.testclient import TestClient
from src.hello import app

client = TestClient(app)


def test_hello():
    response = client.get("/hello")
    assert response.status_code == 200
    assert response.text == '"Hello, MLOps!"'
