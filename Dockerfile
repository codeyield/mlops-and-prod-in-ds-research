FROM python:3.10-slim

WORKDIR /app

COPY poetry.lock pyproject.toml ./

RUN python -m pip install --no-cache-dir poetry==1.8.2 \
    && poetry config virtualenvs.create false \
    && poetry install --without dev --no-interaction \
    && rm -rf $(poetry config cache-dir)/{cache,artifacts}

COPY src/hello.py ./

CMD ["uvicorn", "hello:app", "--host", "0.0.0.0", "--port", "80"]
