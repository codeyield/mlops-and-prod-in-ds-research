# To launch, use: streamlit run src/streamlit/app.py

import streamlit as st
import requests

PREDICTION_API_URL = "http://localhost:8000/api/predict/"


st.title("Sentiment predictor chat")
st.markdown("This app requires running the prediction model inference, which can be " +\
            "found in [this repository](https://github.com/codeyield/mlservice).")

# Initialize chat history
if "messages" not in st.session_state:
    st.session_state.messages = []

# Display chat messages from history on app rerun
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

# Accept user input
if prompt := st.chat_input("Hey, what's new?"):
    
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})
    
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)

    # Making a request to the sentiment prediction model via REST API
    response = requests.post(PREDICTION_API_URL, json={'text': prompt})

    if response.status_code == 200:
        resp_dict = dict(response.json())
        label, score = resp_dict.get('label'), str(resp_dict.get('score'))
        answer = f"Sentiment: **{label}**  \nScore: **{score}**"
    else:
        answer = f"Error during request with status code: {response.status_code}"

    # Display sentiment predictor response in chat message container
    with st.chat_message("assistant"):
        st.markdown(answer)
    
    # Add sentiment predictor response to chat history
    st.session_state.messages.append({"role": "assistant", "content": answer})
