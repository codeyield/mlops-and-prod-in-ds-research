from dataclasses import dataclass

@dataclass
class Paths:
    log: str
    data: str

@dataclass
class Files:
    train_data: str

@dataclass
class Params:
    validation_split: float
    epoch_count: int
    batch_size: int

@dataclass
class TreesConfig:
    paths: Paths
    files: Files
    params: Params
