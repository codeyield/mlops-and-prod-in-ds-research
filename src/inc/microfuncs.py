import os
import random
import string
import matplotlib.pyplot as plt

import multiprocessing

from typing import Any
from typeguard import typechecked, typeguard_ignore

class Microfuncs:
    
    @staticmethod
    def replace_file_extension(file_path:str, extto: str) -> str:
        """
        Replaces the file name extension

        Args:
            file_path (str): Full file name with or without path
            extto (str): New file extension to replace (with or without a dot)

        Returns:
            str: File name with extension with path
        """
        if not extto.startswith("."):
            extto = "." + extto

        path, filename = os.path.split(file_path)
        name, ext = os.path.splitext(filename)
        
        if ext in ['.gz', '.gzip']:
            name, _ = os.path.splitext(name)

        return os.path.join(path, name + extto)


    @staticmethod
    def collect_filename(file_path:str, suffix:str='', ext:str = '') -> str:
        """
        Collects the file name with an optional suffix (if specified) and extension

        Args:
            file_path (str): Full file name with or without path
            suffix (str, optional): Arbitrary string to append to the file name on the right side. Defaults to ''.
            ext (str, optional): New file extension WITH A DOT at the beginning. Defaults to ''.

        Returns:
            str: File name with suffix and/or extension WITHOUT path
        """
        _, filename = os.path.split(file_path)
        name, ext1 = os.path.splitext(filename)
        
        if ext1 in ['.gz', '.gzip']:
            name, _ = os.path.splitext(name)
        
        if len(ext) == 0 and len(suffix) > 0:
            ext, suffix = suffix, ""
        
        return f"{name}{suffix}{ext}"


    @staticmethod
    def generate_random_string(length:int=6) -> str:
        """
        Generates a random character string of lowercase alphabet letters and/or numbers

        Args:
            length (int, optional): Generation string length. Defaults to 6.

        Returns:
            str: Generated string
        """
        chars = string.ascii_lowercase + string.digits
        return ''.join(random.choice(chars) for _ in range(length))


    @staticmethod
    def replace_all(text: str, dic: dict) -> str:
        """
        Performs substitutions in text using the specified substitution dictionary

        Args:
            text (str): Text in which substitutions are made
            dic (dict): Dictionary of species substitutions: {'From': 'To', ...}

        Returns:
            str: Text with substitutions made
        """
        for i, j in dic.items():
            text = text.replace(i, j)
        return text


    @staticmethod
    @typechecked
    def save_plot_to_file(history: Any, imagefile: str, figsize:tuple=(8,4)) -> None:
        """
        Saves the epoch graph of the keras.Sequential model, created via matplotlib.pyplot, to a file

        Args:
            history (Any): Object generated by model.fit(), where model = keras.Sequential()
            imagefile (str): Image filename to save
            figsize (tuple, optional): Plot size as a matplotlib figsize. Defaults to (8,4).
        """
        # Disabling type checking for history
        typeguard_ignore(history)
        
        # Checking that history has the required attributes and methods
        assert hasattr(history, 'history') and isinstance(history.history, dict)
        assert 'accuracy' in history.history
        assert 'loss' in history.history
        assert 'val_accuracy' in history.history
        assert 'val_loss' in history.history

        # Graphing
        plt.figure(figsize=figsize)
        plt.subplot(1, 2, 1)
        plt.plot(history.history['accuracy'], label='Accuracy in training')
        plt.plot(history.history['val_accuracy'], label='Accuracy in validation')
        plt.title('Accuracy')
        plt.xlabel('Epoch')
        plt.ylabel('Accuracy')
        plt.legend()

        plt.subplot(1, 2, 2)
        plt.plot(history.history['loss'], label='Training loss')
        plt.plot(history.history['val_loss'], label='Validation loss')
        plt.title('Loss')
        plt.xlabel('Epoch')
        plt.ylabel('Loss')
        plt.legend()

        plt.tight_layout()

        # Saving the graph to a file
        plt.savefig(imagefile)
        return


    @staticmethod
    def get_cpu_cores() -> int:
        """
        Gets the number of processor cores

        Returns:
            int: Number of processor cores
        """
        # Define the OS
        if os.name == 'nt':  # Windows
            return os.environ.get('NUMBER_OF_PROCESSORS', multiprocessing.cpu_count())
        else:  # Linux, Unix, etc.
            try:
                from os import sched_getaffinity
                return len(sched_getaffinity(0))
            except ImportError:
                return os.cpu_count()
