"""
    Зачищает датасет от сильно прореженных столбцов и от строк с хотя бы одним пустым значением

    Запуск из консоли в корне проекта:
        python src/tree-data-cleaner.py
"""
import pandas as pd
import logging



DATA_FOLDER = 'data'                                # Имя папки с данными от корня проекта
IN_FILE = '2015-street-tree-census-tree-data.csv'   # Файл с исходным датасетом
OUT_FILE = "tree-data-clean.csv"                    # Файл с чистыми данными


def main():

    # Создаем логгер и устанавливаем уровень логгирования
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    # Создаем обработчик для вывода логов в консоль
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    handler.setFormatter(formatter)
    logger.addHandler(handler)


    # Загружаем полный исходный датасет
    data = pd.read_csv(f'{DATA_FOLDER}/{IN_FILE}')
    logger.info(f'Размерность входных данных: {data.shape}')


    # Чистим от столбцов, содержащих много пустых значений
    data = data.drop(['steward', 'guards', 'problems'], axis=1)

    num_rows_with_nulls = data.isnull().any(axis=1).sum()
    logger.info(f"Строк с пустыми значениями к удалению: {num_rows_with_nulls}")


    # Удалим строки с хотя бы одним пустым значением
    data = data.dropna(how='any')
    logger.info(f'Размерность после удаления пропусков: {data.shape}')

    # Оставляем половину датасета для облегчения расчетов
    # sample_size = int(len(data) / 2)
    # data_half = data.sample(n=sample_size, random_state=42)
    # data = data_half
    # logger.info('Оставляем половину от исходного датасета!')
    # logger.info(f'Размерность уполовиненных данных: {data_half.shape}')

    result_file = f'{DATA_FOLDER}/{OUT_FILE}'
    data.to_csv(result_file, index=False)
    logger.info(f"Чистый датасет сохранен в файл {result_file}")


if __name__ == "__main__":
    main()
