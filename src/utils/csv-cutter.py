"""
    Режет текстовый или csv-файл на равные части построчно.

    Запуск из консоли в корне проекта:
    
    python src/csv-cutter.py --file path/filename.csv --parts N
        
        Аргументы:
        --file path/filename.csv    - Входной файл csv (обязательно)
        --parts N                   - Кол-во частей датасета в диапазоне 1..100
        
        Выход:
        Серия файлов с именами filename-01.csv, filename-02.csv, ... - и т.д. по кол-ву частей
"""
import os
import glob
import logging
import argparse
import pandas as pd
# import math


def main():

    # Создаем логгер и устанавливаем уровень логгирования
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    # Создаем обработчик для вывода логов в консоль
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)


    # Получаем аргументы из команды запуска скрипта
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--file", type=str, required=True, default='', help="Укажите входной файл csv")
    parser.add_argument("--parts", type=int, default=1, help="Укажите кол-во частей данных в диапазоне 1..99")
    args = parser.parse_args()


    # Кол-во частей приводим к допустимому диапазону
    args.parts = min(max(args.parts, 1), 99)


    # Элементы для сборки
    directory, full_filename = os.path.split(args.file)
    filename, extension = os.path.splitext(full_filename)

    
    # Удаляем файлы по маске, оставшиеся от предыдущих запусков
    mask = os.path.join(directory, f"{filename}-*{extension}")
    for name in glob.glob(mask):
        os.remove(name)
    
    logger.info(f'Удалены выходные файлы от предыдущих запусков: {mask}')


    # Читаем входной csv в датафрейм
    data = pd.read_csv(args.file)
    logger.info(f'Загружен csv-файл: {args.file}')
    logger.info(f'Размерность входных данных: {data.shape}')

    
    # Делим данные на равные части построчно и сохраняем каждую часть в отдельный файл

    # Определяем размер каждой части
    # part_size = math.ceil(len(data) / args.parts)
    part_size = (len(data) + args.parts - 1) // args.parts


    # Разделяем датафрейм на части
    parts = [data.iloc[i:i+part_size] for i in range(0, len(data), part_size)]


    # Сохраняем каждую часть в отдельный csv-файл
    for i, part in enumerate(parts, start=1):
        part_filename = os.path.join(directory, f"{filename}-{i:02d}{extension}")
        
        part.to_csv(part_filename, index=False)
        logger.info(f"Часть {i}, размерность {part.shape} сохранена в файл {part_filename}")


if __name__ == "__main__":
    main()
