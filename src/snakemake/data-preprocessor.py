import pandas as pd


# Empty data frame for merged data
combined_df = pd.DataFrame()

# Reading and merging CSV files into one DataFrame
for file in snakemake.input:
    df = pd.read_csv(file, low_memory=False)
    # combined_df = pd.concat([combined_df, df], ignore_index=True)
    combined_df = pd.concat(
        [combined_df.dropna(how="all", axis=1), df.dropna(how="all", axis=1)],
        ignore_index=True,
    )

# Saving the merged DataFrame to a new CSV file
combined_df.to_csv(snakemake.output[0], index=False)
