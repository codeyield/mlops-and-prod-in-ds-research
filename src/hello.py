"""
Hello MLOps модуль. Выводит в браузер сообщение по адресу http://localhost:8080/hello
"""

from fastapi import FastAPI

app = FastAPI()


@app.get("/hello")
def hello():
    """
    Возвращает "Hello MLOps"
    """
    return "Hello, MLOps!"
