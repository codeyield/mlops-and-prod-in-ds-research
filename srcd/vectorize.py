import os
from pathlib import Path

import click
import dvc.api
import joblib
import numpy as np
import pandas as pd
import scipy as sp
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from collections import OrderedDict

from cli import cli


@cli.command()
@click.argument("input_frame_path", type=Path)
@click.argument("vectorizer_path", type=Path)
@click.argument("train_features_path", type=Path)
@click.argument("val_features_path", type=Path)
def cli_vectorize_split(
    input_frame_path: Path,
    vectorizer_path: Path,
    train_features_path: Path,
    val_features_path: Path,
):
    data = pd.read_csv(input_frame_path)
    vectorizer, train, val = vectorize_split(data)
    
    # Создаем выходные папки, если не существуют
    os.makedirs(os.path.split(vectorizer_path)[0], exist_ok=True)
    os.makedirs(os.path.split(train_features_path)[0], exist_ok=True)
    os.makedirs(os.path.split(val_features_path)[0], exist_ok=True)

    joblib.dump(vectorizer, vectorizer_path)
    train.to_csv(train_features_path)
    val.to_csv(val_features_path)


def vectorize_split(
    data: pd.DataFrame,
) -> tuple[TfidfVectorizer, pd.DataFrame, pd.DataFrame]:
    
    # Загрузка параметров модели из params.yaml
    params = dvc.api.params_show()
    
    tfidf_vectorizer = TfidfVectorizer(**params["vectorizer_tfidf"])

    data = data.dropna()
    train, val = train_test_split(
        data,
        test_size=params["test_train_split"],
        random_state=params["random_state"],
        shuffle=False,
    )
    
    tfidf_vectorizer.fit([train["corpus"].str.cat(sep=' ')])

    # Упорядочивание словаря в векторайзере TF/IDF для улучшения воспроизводимости
    tfidf_vectorizer.vocabulary_ = OrderedDict(sorted(tfidf_vectorizer.vocabulary_.items(), key=lambda kv: kv[1]))
    tfidf_vectorizer._stop_words_id = 0

    return tfidf_vectorizer, train, val


@cli.command()
@click.argument("input_frame_path", type=Path)
@click.argument("vectorizer_path", type=Path)
@click.argument("result_frame_path", type=Path)
@click.argument("target_frame_path", type=Path)
def cli_vectorize_apply(
    input_frame_path: Path,
    vectorizer_path: Path,
    result_frame_path: Path,
    target_frame_path: Path,
):
    data = pd.read_csv(input_frame_path)
    vectorizer = joblib.load(vectorizer_path)
    result = vectorize_apply(vectorizer, data)
    
    # Создаем выходные папки, если не существуют
    os.makedirs(os.path.split(result_frame_path)[0], exist_ok=True)
    os.makedirs(os.path.split(target_frame_path)[0], exist_ok=True)

    sp.sparse.save_npz(result_frame_path, result)
    np.save(target_frame_path, data["Polarity"].to_numpy())


def vectorize_apply(vectorizer: TfidfVectorizer, data: pd.DataFrame) -> pd.DataFrame:
    return vectorizer.transform(data['corpus'])
