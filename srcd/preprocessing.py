import os
import re
from pathlib import Path

import click
import dvc.api
import nltk
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

from cli import cli

nltk.download('wordnet')
nltk.download("stopwords")


@cli.command()
@click.argument("input_frame_path", type=Path)
@click.argument("output_frame_path", type=Path)
@click.argument("column", type=str, default="Review")
def cli_preprocessing(input_frame_path: Path, output_frame_path: Path, column: str):
    
    # Загрузка параметров модели из params.yaml
    params = dvc.api.params_show()

    data = pd.read_csv(
        input_frame_path,
        compression='gzip', 
        header=None, 
        names=["Polarity", "Title", "Review"],
        nrows=params["rows_to_load_from_dataset"], 
    )
    processed_data = dataframe_preprocessing(data, column)

    # Создаем выходную папку, если не существует
    os.makedirs(os.path.split(output_frame_path)[0], exist_ok=True)
    processed_data.to_csv(output_frame_path)


# Предварительная компиляция шаблонов регулярок для ускорения
stop_words = set(stopwords.words("english"))
url_pattern = re.compile(r"https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*")
spec_chars_pattern = re.compile("[0-9 \-_]+")
non_alpha_pattern = re.compile("[^a-z A-Z]+")


def text_preprocessing(input_text: str) -> str:
    text = input_text.lower()
    text = url_pattern.sub("", text)
    text = spec_chars_pattern.sub(" ", text)    # убираем спец символы
    text = non_alpha_pattern.sub(" ", text)     # оставляем только буквы
    text = " ".join(word for word in text.split() if word not in stop_words)
    return text.strip()


def lemmatize(input_frame: pd.DataFrame) -> pd.DataFrame:
    lemmatizer = WordNetLemmatizer()
    input_frame['corpus'] = input_frame['corpus'].apply(lambda input_list: [lemmatizer.lemmatize(token) for token in input_list])
    return input_frame


def dataframe_preprocessing(data: pd.DataFrame, col_name: str) -> pd.DataFrame:
    data[col_name] = data[col_name].apply(text_preprocessing)
    data['corpus'] = data[col_name].str.split(" ")
    return lemmatize(data)
