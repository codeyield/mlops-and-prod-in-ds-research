import os
import json
from pathlib import Path

import click
import dvc.api
import joblib
import numpy as np
import scipy as sp
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import (
    ConfusionMatrixDisplay,
    classification_report,
)

from cli import cli


# Обучение модели
@cli.command()
@click.argument("train_frame_path", type=Path)
@click.argument("train_target_path", type=Path)
@click.argument("model_path", type=Path)
def cli_model_train(
    train_frame_path: Path,
    train_target_path: Path,
    model_path: Path,
):
    train_features = sp.sparse.load_npz(train_frame_path)
    train_target = np.load(train_target_path)
    model = train(train_features, train_target)

    # Создаем выходную папку, если не существует
    os.makedirs(os.path.split(model_path)[0], exist_ok=True)
    joblib.dump(model, model_path)


def train(data: np.ndarray, target: np.ndarray) -> LogisticRegression:
    params = dvc.api.params_show()
    
    model_lr = LogisticRegression(**params["logistic_regression"])
    model_lr.fit(data, target)
    return model_lr


# Проверка предсказания на тестовых данных
@cli.command()
@click.argument("test_frame_path", type=Path)
@click.argument("test_target_path", type=Path)
@click.argument("model_path", type=Path)
@click.argument("metric_path", type=Path)
@click.argument("figure_path", type=Path)
def cli_model_test(
    test_frame_path: Path,
    test_target_path: Path,
    model_path: Path,
    metric_path: Path,
    figure_path: Path,
):
    test_features = sp.sparse.load_npz(test_frame_path)
    test_target = np.load(test_target_path)
    model = joblib.load(model_path)
    result, fig = model_test(model, test_features, test_target)

    # Создаем выходные папки, если не существуют
    os.makedirs(os.path.split(metric_path)[0], exist_ok=True)
    os.makedirs(os.path.split(figure_path)[0], exist_ok=True)

    json.dump(result, metric_path.open("w"))
    plt.savefig(figure_path)


def model_test(model: LogisticRegression, data: np.ndarray, target: np.ndarray) -> tuple[dict, Figure]:
    predict = model.predict(data)
    fig = confl_matrix(target, predict)
    report = classification_report(target, predict, output_dict=True)
    return (round_floats_in_dict(report), fig)


def confl_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5, 5))
    ConfusionMatrixDisplay.from_predictions(y_true, pred, ax=ax, colorbar=False)
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title("Conflusion matrix")
    plt.tight_layout()
    return fig


# Округляет рекурсивно все float значения в словаре до указанной точности
def round_floats_in_dict(data: dict, precision:int=5) -> dict:
    if isinstance(data, dict):
        rounded_dict = {}
        for key, value in data.items():
            rounded_dict[key] = round_floats_in_dict(value, precision)
        return rounded_dict
    elif isinstance(data, list):
        return [round_floats_in_dict(item, precision) for item in data]
    elif isinstance(data, float):
        return round(data, precision)
    else:
        return data
