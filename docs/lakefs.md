# Отчет по заданию LakeFS

Подготовлен Docker compose [образ](../lakefs/docker-compose.yml), включающий сборку LakeFS, minIO (AWS S3 эмулятор) и БД Postgres.

Реализован Workflow [пайплайн](../lakefs/snakefile.smk) с использованием фреймворка **Snakemake**, демонстрирующий загрузку и версионирование датасета "NY 2015 street tree centus tree data".

После запуска Docker контейнера были сделаны следующие настройки:

## Конфигурация AWS (minIO)

`aws configure` (указать секреты из конфига [Docker-compose](../lakefs/docker-compose.yml): `minioadmin / minioadmin`)

Создаем бакет с именем `data`:

`aws s3 mb s3://data`

Задаем [локальный endpoint](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-endpoints.html):

`export AWS_ENDPOINT_URL=http://localhost:9000` (Linux)\
`$Env:AWS_ENDPOINT_URL="http://localhost:9000"` (Windows Power Shell)

## Конфигурация LakeFS

`lakectl config` (указать секреты, выведенные LakeFS при первом запуске)

Создаем репозиторий:

`lakectl repo create lakefs://data s3://data`

## Запуск пайплайна

Пример запуска workflow пайплайна Snakemake (из корневой папки проекта):

`snakemake --cores 2 --snakefile lakefs/snakefile.smk`
