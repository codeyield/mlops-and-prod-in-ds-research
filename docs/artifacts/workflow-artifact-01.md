## Артефакт обучения ML-модели

_Артефакт создан как часть workflow процесса обработки данных. Обучение модели носит демонстрационный характер._

Входной файл: **data/compiled-data-01.csv**

Достигнутая точность: **0.791**

![](artifacts/workflow-artifact-01.png)
