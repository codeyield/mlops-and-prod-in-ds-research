## Артефакт обучения ML-модели

_Артефакт создан как часть workflow процесса обработки данных. Обучение модели носит демонстрационный характер._

Входной файл: **data\tree-data-clean-half.csv.gz**

Достигнутая точность: **0.802**

![](artifacts/tree-data-clean-half-artifacts-0erwj6.png)
