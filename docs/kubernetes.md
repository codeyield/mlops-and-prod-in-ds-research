# Отчет по заданию Kubernetes

Развернут простой Kubernetes кластер на Minikube. Кластер включает:

1. [ConfigMap](../kubernetes/01_configmap.yml) с конфигурацией Nginx и содержимым файла index.html
2. [Deployment](../kubernetes/02_deployment.yml) с двумя репликами подов простого веб-приложения на базе образа Nginx
3. [Service](../kubernetes/03_service.yml), выполняющий роль балансировщика, и отправляющий трафик на поды

*NB: Конфигурация кластера носит учебный характер и не предназначена для Production окружения. Для прода потребуется (как вариант) задеплоить и настроить Ingress Controller для внешнего трафика, а с него направить трафик на поды, управляемые Service NodePort.*

## Развертывание

После установки Minikube на локальной машине активируйте следующие аддоны:

`minikube addons enable dashboard`\
`minikube addons enable storage-provisioner`\
`minikube addons enable metrics-server`

Далее следует применить все манифесты из папки `/kubernetes` в порядке нумерации:

`cd kubernetes`\
`kubectl apply -f 01_configmap.yml`\
`kubectl apply -f 02_deployment.yml`\
`kubectl apply -f 03_service.yml`

Или то же самое:

`cd kubernetes`\
`kubectl apply -f .`

## Мониторинг

После активации соответствующего аддона можно использовать браузерную панель мониторинга "из коробки":

`minikube dashboard`

Другим удобным opensource инструментом является [Headlamp](https://github.com/headlamp-k8s/headlamp) (аналог Lens), доступный также в виде десктопного клиента. Настройка достаточно проста:

`kubectl -n kube-system create serviceaccount headlamp-admin`\
`kubectl create token headlamp-admin -n kube-system`

Распечатанный в терминал токен следует ввести в диалоговое окно, открывшееся после запуска Headlamp.

## Проброс портов 

Проверим, что балансировщик получил внешнй IP-адрес:

`kubectl get svc`
```
NAME         TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
kubernetes   ClusterIP      10.96.0.1       <none>        443/TCP        62m
nginx        LoadBalancer   10.99.139.161   127.0.0.1     80:31687/TCP   61m
```

Запустим туннель в отдельном окне терминала:

`minikube tunnel`

## Проверка результата

Добавим запись домена в локальный файл `hosts`:

`127.0.0.1 mlops.example.com`

И откроем этот URL в браузере:

![](../docs/screenshots/minikube-hello-mlops.png)
