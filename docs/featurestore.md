# Отчет по заданию Feature Store

Развернут локальный инстанс FeatureForm из коробочной сборки Docker, которая включает провайдеров Postgres и Redis. FeatureForm используется здесь для препроцессинга датасета и сохранения его для последующего использования в задаче машинного обучения.

## Подготовка среды

Документация рекомендует устанавливать FeatureForm через `pip`:

`cd featureform`\
`pip install featureform`\
`pip install googleapis-common-protos`\
`pip install pandas sqlalchemy psycopg2-binary python-dotenv`

Также можно дополнительно установить пакет `ipywidgets` для отображения сообщений FeatureForm в Jupyter Notebooks.

## Установка Docker-сборки

`featureform deploy docker --quickstart`

Опция "quickstart" создаст файлы базовой настройки конфигурации и демо-примеры в папке `/quickstart`. Рекомендуется это сделать, хотя использовать их необязательно. Далее обязательно установите переменную среды, иначе возникнут ошибки:

`export FEATUREFORM_HOST=localhost:7878` (linux)\
`$Env:FEATUREFORM_HOST="localhost:7878"` (PowerShell)

Примените настройки для демо-примеров, если собираетесь использовать демо:

`featureform apply --insecure quickstart/definitions.py`

Альтернативно, все необходимые настройки для проекта можно выполнить, запустив собственный файл конфигурации или ноутбук.

## Описание задачи

Для демонстрации использования **FeatureForm** выбран датасет **ny-2015-street-tree-census-tree-data**. Скрипты размещены в папке `/featureform`.

Скрипт [dataset_to_postgres.py](../featureform/dataset_to_postgres.py) запускается единожды и загружает csv-файл с датасетом в отдельную таблицу Postgres. В данном примере Postgres используется в качестве data lake, хотя в промышленных решениях целесообразно использовать более подходящих провайдеров для хранения сырых данных.

Основная реализация Feature Store выполнена в ноутбуке [workflow.ipynb](../featureform/workflow.ipynb). 
