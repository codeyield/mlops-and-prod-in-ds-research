# Отчет по заданию ClearML

Применен базовый функционал **ClearML** в задаче версионирования эксперимента для двух различных ML-моделей. Выполнялась сравнительная задача машинного обучения по классификации отзывов из датасета [Amazon reviews](https://www.kaggle.com/datasets/kritanjalijain/amazon-reviews) (оценка сентимента).

Ноутбуки с исследованиями:

- Модель №1: [Частотный анализ на базе TF-IDF](../notebooks/clearml/clml_tfidf_amazon.ipynb), ссылка на [эксперимент TF-IDF](https://app.clear.ml/projects/632d87a87e714a7fbc73c21e83eebfa5/experiments/6b29acc710494218bd58f58b7e0f5f25/output/execution)
- Модель №2: [Эмбеддинг на базе Bert Transformer](../notebooks/clearml/clml_bert_amazon.ipynb), ссылка на [эксперимент Bert](https://app.clear.ml/projects/632d87a87e714a7fbc73c21e83eebfa5/experiments/45554c2750e54c0bb5fd93f6ed64ffa7/output/execution)

Для выполнения задачи использовалась облачная SaaS-версия **ClearML**. Были проведены сравнительные эксперименты для выбранных моделей, зафиксированы результаты и составлен данный отчет. Модель Bert по всем метрикам показала лучшие результаты.

В ноутбуках использовался фреймворк **Hydra** для загрузки параметров. Промежуточные результаты, такие как веса TF-IDF Vectorizer и Logistic Regression, также были сохранены в облаке как сериализованные артефакты.

## Подготовка среды

Пример настройки среды для **conda/mamba**:

`mamba create -y -n clearml python=3.10.14 numpy pandas polars clearml scikit-learn matplotlib pytorch transformers hydra-core omegaconf ipykernel jupyter`

## Подготовка и загрузка данных в ClearML

Для авторизации на SaaS-платформе необходимо получить секреты в настройках личного профиля, после чего добавить их в переенные среды любым удобным способом. Пример для Windows Power Shell:

`$env:CLEARML_WEB_HOST="https://app.clear.ml"`\
`$env:CLEARML_API_HOST="https://api.clear.ml"`\
`$env:CLEARML_FILES_HOST="https://files.clear.ml"`\
`$env:CLEARML_API_ACCESS_KEY="XXXXXXXXXXXXXXXX"`\
`$env:CLEARML_API_SECRET_KEY="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"`

Загрузка датасета в облако **ClearML** с указанием параметров, которые затем будут использованы в ноутбуке:

`clearml-data create --project "Amazon reviews" --name "Raw data (first 50K)"`\
`clearml-data add --files datad/train50k.csv`\
`clearml-data close`

*NB: В целях сокращения затрат на загрузку/выгрузку данных, исходный датасет Amazon reviews (1.5Тб) был обрезан до фактически используемого в обучении моделей размера.*

Далее выполнялся запуск обоих ноутбуков с автоматическим логированием сохранением артефактов в облаке **ClearML**.
